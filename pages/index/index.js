// index.js
const baseUrl = getApp().globalData.baseUrl, // domain
    W = wx.getSystemInfoSync().windowWidth, // 屏幕宽度
    X = W * 0.094, // 初始坐标x
    Y = W * 0.232, // 初始坐标y
    ratio = wx.getSystemInfoSync().devicePixelRatio; // 设备像素比

Page({
    data: {
        sources: {
            lightSec1Part1: baseUrl + '/index/light-sec1-part1.png',
            titleSec1Part1: baseUrl + '/index/title-sec1-part1.png',
            bgCar: baseUrl + '/index/bg-car.png',
            topCar: baseUrl + '/index/top-car.png',
            blueline: baseUrl + '/index/blueline.png',
            tipSec1Part1: baseUrl + '/index/tip-sec1-part1.png',
            lightSec1Part2: baseUrl + '/index/light-sec1-part2.png',
            titleSec1Part2: baseUrl + '/index/title-sec1-part2.png',
            lightCar: baseUrl + '/index/light-car.png',
            tipSec1Part2: baseUrl + '/index/tip-sec1-part2.png',
            lightSec2: baseUrl + '/index/light-sec2.png',
            btnPlay: baseUrl + '/index/btn-play.png',
            logo: baseUrl + '/index/logo2.png',
            titleSec2: baseUrl + '/index/title-sec2.png',
            subtitleSec2: baseUrl + '/index/subtitle-sec2.png',
            tipSec2: baseUrl + '/index/tip-sec2.png',
            arrow: baseUrl + '/index/arrow.png',
            jpg1: baseUrl + '/index/p3-1.jpg',
            gif1: baseUrl + '/index/p3-1.gif',
            tipSec3Part1: baseUrl + '/index/tip-sec3-part1.png',
            jpg2: baseUrl + '/index/p3-2.jpg',
            gif2: baseUrl + '/index/p3-2.gif',
            tipSec3Part2: baseUrl + '/index/tip-sec3-part2.png',
            jpg3: baseUrl + '/index/p3-3.jpg',
            gif3: baseUrl + '/index/p3-3.gif',
            tipSec3Part3: baseUrl + '/index/tip-sec3-part3.png',
            toast: baseUrl + '/index/toast.png',
            lightSec3Part34: baseUrl + '/index/light-sec3-part34.png',
            titleSec3Part4: baseUrl + '/index/title-sec3-part4.png',
            subtitleSec3Part4: baseUrl + '/index/subtitle-sec3-part4.png',
            tipSec3Part4: baseUrl + '/index/tip-sec3-part4.png',
            btnCadillac: baseUrl + '/index/btn-cadillac.png',
            titleSec3Part5: baseUrl + '/index/title-sec3-part5.png?_v=0jfwufa',
            subtitleSec3Part5: baseUrl + '/index/subtitle-sec3-part5.png',
            tipSec3Part5: baseUrl + '/index/tip-sec3-part5.png',
            btnHummer: baseUrl + '/index/btn-hummer.png',
            iconLogo: baseUrl + '/index/icon-logo.png',
            more: baseUrl + '/index/more.png',
            copyright: baseUrl + '/index/copyright.png'
        },
        anAll: {}, // 整体动画
        index: 0, // swiper
        supportCanvas: true, // 是否支持canvas
        part1Hide: false, // sec1 part1
        whitelinew: 0.094 * 100, // 白线
        ctx: null, // canvas
        boundl: X, // 移动左边界
        boundr: W * 0.94, // 移动右边界
        bound: W * 0.86, // 移动范围 
        path1: {
            start: [X, Y],
            point: [W * 0.18, W * 0.206],
            end: [W * 0.26, W * 0.2]
        }, // 第一段路径
        path2: {
            start: [W * 0.26, W * 0.2],
            point: [W * 0.31, W * 0.198],
            end: [W * 0.352, W * 0.174]
        }, // 第二段路径
        path3: {
            start: [W * 0.352, W * 0.174],
            point: [W * 0.536, W * 0.058],
            end: [W * 0.83, W * 0.172]
        }, // 第三段路径
        path4: {
            start: [W * 0.83, W * 0.172],
            point: [W * 0.9, W * 0.19],
            end: [W * 0.94, W * 0.178]
        }, // 第四段路径
        point: {
            x: X,
            y: Y
        }, // 移动点的坐标
        circle: {
            x: X, // 圆的中心坐标x
            y: Y, // 圆的中心坐标y
            radius: 12, // 圆半径
            sAngle: 0, // 起始角度
            eAngle: Math.PI * 2, // 终结角度
            color: '#fff', // 填充色
            width: 20, // 描边宽度
            border: 'rgba(255, 255, 255, 0.2)' // 描边色
        },
        canAn: true, // 是否可以开始移动
        percent: 0, // 进程
        count: 0, // 动画次数
        isAnDone: false, // 动画是否结束
        isDown: false, // 是否按住
        dx: 0, // 点击点x距离圆点x的距离
        isDone: false, // 是否完成
        anCar1: {}, // 淡出动画
        anPlayer1: {}, // 视频动画
        player1: { // 视频1
            poster: '',
            src: baseUrl + '/media/player1.mp4',
            controls: false, // 控件
            btnplay: false, // 播放按钮
            muted: true, // 静音
            autoplay: false, // 自动播放
            loop: true, // 循环
            progress: false // 控制进度的手势
        },
        anCar2: {}, // 淡入动画
        player2: { // 视频2
            poster: baseUrl + '/index/poster2.jpg',
            src: baseUrl + '/media/player90s.mp4',
            controls: false, // 控件
            btnplay: false, // 中间播放按钮
            mute: true, // 静音
            fullscreen: false, // 全屏
            autoplay: false, // 自动播放
            loop: false, // 循环
            progress: true, // 控制进度的手势
            isPlaying: false // 是否在播放
        },
        anGif1: {}, // gif淡入
        anGif2: {},
        anGif3: {},
        toastHide: true,
        player3: { // 视频3
            poster: '',
            src: baseUrl + '/media/cadillac.mp4',
            controls: false, // 控件
            btnplay: false, // 播放按钮
            muted: true, // 静音
            autoplay: false, // 自动播放
            loop: true, // 循环
            progress: false // 控制进度的手势
        },
        player4: { // 视频4
            poster: '',
            src: baseUrl + '/media/hummer.mp4',
            controls: false, // 控件
            btnplay: false, // 播放按钮
            muted: true, // 静音
            autoplay: false, // 自动播放
            loop: true, // 循环
            progress: false // 控制进度的手势
        },
        track: { // 监测
            section1: false,
            section2: false,
            touchmove: false,
            section3part1: false,
            section3part2: false,
            section3part3: false,
            section3part4: false,
            section3part5: false
        }
    },
    onLoad() {
        wx.showLoading({title: '加载中'});
        let animation = wx.createAnimation();
        this.animation = animation;
        setTimeout(() => {this.anAllStart();}, 1500);
    },
    onReady() {
        const query = wx.createSelectorQuery();
        query.select('#btnCanvas').fields({node: true, size: true});
        query.exec((res) => {
            // canvas
            const canvas = res[0].node;
            let time = 0,
                getCanvas = setInterval(() => {
                    time += 1;
                    if(getApp().isset(canvas)) {
                        // 支持canvas
                        clearInterval(getCanvas);
                        canvas.width = W * ratio;
                        canvas.height = W * 0.36 * ratio;
                        this.setData({ctx: canvas.getContext('2d')});
                        let ctx = this.data.ctx; 
                        ctx.scale(ratio, ratio);
                        // 初始圆
                        ctx.beginPath();
                        ctx.fillStyle = this.data.circle.color;
                        ctx.arc(this.data.circle.x, this.data.circle.y, this.data.circle.radius, this.data.circle.sAngle, this.data.circle.eAngle);
                        ctx.lineWidth = this.data.circle.width;
                        ctx.strokeStyle = this.data.circle.border;
                        ctx.closePath();
                        ctx.fill();
                        ctx.stroke();
                    };
                    if(time == 4) {
                        // 不支持canvas
                        clearInterval(getCanvas);
                        this.setData({
                            supportCanvas: false,
                            part1Hide: true,
                            canAn: false,
                            isAnDone: true
                        });
                    };
                }, 100);
            // player autoplay
            this.playerObserve('player1');
            this.playerObserve('player2');
            this.playerObserve('player3');
            this.playerObserve('player4');
            // track
            if(!this.data.track.section1) {
                getApp().mtj.trackEvent('section1');
                this.setData({['track.section1']: true});
            };
            this.trackObserve('part2');
            this.trackObserve('part3');
            this.trackObserve('part4');
            this.trackObserve('part5');
        });
    },
    onShow() {
        if(this.data.anAll == null && !this.data.isAnDone && this.data.index == 0) {
            this.setData({canAn: true});
            setTimeout(() => {this.guide();}, 600);
        };
    },
    onHide() {
        if(!this.data.isAnDone && this.data.index == 0) {
            this.setData({canAn: false});
        };
    },
    // an all
    anAllStart() {
        this.animation.opacity(1).step({duration: 300, timingFunction: 'ease-out'});
        this.setData({anAll: this.animation.export()});
    },
    anAllEnd() {
        wx.hideLoading();
        this.setData({anAll: null});
        if(this.data.supportCanvas) {
            this.checkAn();
        } else {
            this.anCar1End();
        };
    },
    // check
    checkAn() {
        if(!this.data.isAnDone) {
            if(this.data.index == 0) {
                if(!this.data.canAn) {this.setData({canAn: true});};
                setTimeout(() => {this.guide();}, 600);
            } else {
                this.setData({canAn: false});
            };
        };
    },
    checkTrack(index) {
        if(index == 1 && !this.data.track.section2) {
            getApp().mtj.trackEvent('section2');
            this.setData({['track.section2']: true});
            let player = wx.createVideoContext('player2', this);
            setTimeout(() => {player.play();}, 150);
        };
        if(index == 2 && !this.data.track.section3part1) {
            getApp().mtj.trackEvent('section3part1');
            this.setData({['track.section3part1']: true});
        };
    },
    // swiper
    swiperTo() {
        let nindex = this.data.index + 1;
        this.setData({index: nindex});
        this.checkTrack(nindex);
        getApp().mtj.trackEvent('slideclick' + nindex);
    },
    swiperChange(e) {
        let current = e.detail.current;
        if(this.data.index != current) {
            this.checkTrack(current);
            if(current == (this.data.index + 1)) {
                getApp().mtj.trackEvent('slide' + current);
            };
            this.setData({index: current});
            this.checkAn();
        };
    },
    // 二次贝塞尔曲线方程
    quadraticBezier(p0, p1, p2, t) {
        let k = 1 - t;
        return k * k * p0 + 2 * (1 - t) * t * p1 + t * t * p2;
    },
    // 描绘贝塞尔曲线
    drawCurvePath(start, point, end, percent) {
        for(let t = 0; t <= percent / 100; t += 0.001) {
            let x = this.quadraticBezier(start[0], point[0], end[0], t),
                y = this.quadraticBezier(start[1], point[1], end[1], t);
            this.data.point.x = x;
            this.data.point.y = y;
        };
    },
    // 替代小程序没有的requestAnimationFrame方法
    runAnimationFrame(callback, ctx, path, lastTime) {
        if(typeof lastTime == 'undefined') {lastTime = 0;};
        let currTime = new Date().getTime(),
            timeToCall = Math.max(0, 16.7 - (currTime - lastTime));
        lastTime = currTime + timeToCall;
        let id = setTimeout(() => {
                callback(ctx, path, currTime + timeToCall, lastTime);
            }, timeToCall);
        return id;
    },
    cancelAnimationFrame(id) {
        clearTimeout(id);
    },
    // 二次贝塞尔动画
    anDraw(ctx, path) {
        if(!this.data.isAnDone && this.data.canAn) {
            this.drawCurvePath(path.start, path.point, path.end, this.data.percent);
            this.drawPath(ctx);
            if(this.data.count % 2 == 0) {
                // 前进
                this.data.percent += 0.1; // 进程增加,这个控制动画速度
                if(this.data.percent < 100) { // 没有画完接着调用,画完的话重置进度
                    this.runAnimationFrame(this.anDraw, ctx, path);
                } else {
                    this.data.count += 1;
                    this.guide();
                };
            } else {
                // 后退
                this.data.percent -= 0.1; // 进程增加,这个控制动画速度
                if(this.data.percent > 0) { // 没有画完接着调用,画完的话重置进度
                    this.runAnimationFrame(this.anDraw, ctx, path);
                } else {
                    this.data.count += 1;
                    this.guide();
                };
            };
        };
    },
    guide() {
        this.anDraw(this.data.ctx, this.data.path1);
    },
    draw(ctx, path, percent) {
        this.drawCurvePath(path.start, path.point, path.end, percent);
        if(this.data.point.x > this.data.circle.x) {
            // 只能前进不能后退
            this.drawPath(ctx);
        };
    },
    drawPath(ctx) {
        this.data.circle.x = this.data.point.x;
        this.data.circle.y = this.data.point.y;
        // 描线
        let nw = this.data.circle.x / W * 100;
        this.setData({whitelinew: nw});
        // 圆按钮
        ctx.clearRect(0, 0, W, W * 0.36);
        ctx.beginPath();
        ctx.fillStyle = this.data.circle.color;
        ctx.arc(this.data.circle.x, this.data.circle.y, this.data.circle.radius, this.data.circle.sAngle, this.data.circle.eAngle);
        ctx.lineWidth = this.data.circle.width;
        ctx.strokeStyle = this.data.circle.border;
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    },
    // touch
    touchStart(e) {
        if(!this.data.track.touchmove) {
            getApp().mtj.trackEvent('touchmove');
            this.setData({['track.touchmove']: true});
        };
        if(!this.data.isDone && (this.data.ctx.isPointInStroke(e.changedTouches[0].x * ratio, e.changedTouches[0].y * ratio) || this.data.ctx.isPointInPath(e.changedTouches[0].x * ratio, e.changedTouches[0].y * ratio))) {
            if(!this.data.isAnDone) {
                this.setData({
                    isAnDone: true,
                    canAn: false
                });
            };
            this.setData({
                isDown: true,
                dx: e.changedTouches[0].x - this.data.circle.x
            });
        };
    },
    touchMove(e) {
        if(this.data.isDown) {
            // 原本应该要到达的坐标
            let nx = e.changedTouches[0].x - this.data.dx;
            if(nx > this.data.circle.x && nx < this.data.boundr) {
                // 距离和时间
                let distance = nx - this.data.boundl,
                t = 0;
                switch(true) {
                    case (distance > 0 && distance <= (this.data.bound * 6 / 28)):
                        t = distance/(this.data.bound * 6 / 28) * 100;
                        this.draw(this.data.ctx, this.data.path1, t);
                        break;
                    case (distance > (this.data.bound * 6 / 28) && distance <= (this.data.bound * 9 / 28)):
                        t = (distance - (this.data.bound * 6 / 28))/(this.data.bound * 3 / 28) * 100;
                        this.draw(this.data.ctx, this.data.path2, t);
                        break;
                    case (distance > (this.data.bound * 9 / 28) && distance <= (this.data.bound * 24 / 28)):
                        t = (distance - (this.data.bound * 9 / 28))/(this.data.bound * 15 / 28) * 100; 
                        this.draw(this.data.ctx, this.data.path3, t);
                        break;
                    case (distance > (this.data.bound * 24 / 28) && distance <= this.data.bound):
                        t = (distance - (this.data.bound * 24 / 28))/(this.data.bound * 4 / 28) * 100; 
                        this.draw(this.data.ctx, this.data.path4, t);
                        break;
                    default: //
                };
            } else if(nx >= this.data.boundr) {
                // 描边完成
                nx = this.data.boundr;
            };
            if(nx >= W * 0.44) {
                // 到达终点
                if(!this.data.isDone) {
                    this.setData({isDone: true});
                };
            };
        };
    },
    touchEnd(e) {
        this.setData({isDown: false});
        if(this.data.isDone) {
            this.anCar1Start();
            this.setData({
                ctx: null,
                boundl: null,
                boundr: null,
                bound: null,
                path1: null,
                path2: null,
                path3: null,
                path4: null,
                point: null,
                circle: null,
                percent: null,
                count: null,
                dx: null
            });
        };
    },
    canvasErrorCallback(e) {
        wx.showToast({title: '不支持canvas', icon: 'error'});
    },
    // an car
    anCar1Start() {
        setTimeout(() => {
            this.animation.opacity(0).step({duration: 1000, timingFunction: 'ease-out'});
            this.setData({anCar1: this.animation.export()});
        }, 300);
    },
    anCar1End() {
        this.setData({part1Hide: true});
        let player = wx.createVideoContext('player1', this);
        if(player) {
            setTimeout(() => {player.play();}, 150);
            this.animation.opacity(1).step({duration: 100, timingFunction: 'step-start'});
            this.setData({
                anCar1: null,
                anPlayer1: this.animation.export()
            });
            setTimeout(() => {
                this.animation.opacity(1).step({duration: 1000, timingFunction: 'ease-out'});
                this.setData({
                    anPlayer1: null,
                    anCar2: this.animation.export()
                });
            }, 2000);
        };
    },
    anCar2End() {
        this.setData({anCar2: null});
    },
    // player event
    player2Play() {
        getApp().mtj.trackEvent('player2');
        let player = wx.createVideoContext('player2', this);
        if(player) {player.play();};
    },
    player2Fun(e) {
        if(e.type == 'play') {
            this.setData({
                ['player2.controls']: true,
                ['player2.isPlaying']: true
            });
        } else {
            this.setData({
                ['player2.controls']: false,
                ['player2.isPlaying']: false
            });
        };
    },
    playerErrorCallback(e) {
        console.log(e.detail.errMsg);
    },
    playerObserve(id) {
        this._observer = this.createIntersectionObserver({observeAll: true});
        this._observer.relativeToViewport({top: 0, bottom: 0}).observe('#' + id, (res) => {
            let player = wx.createVideoContext(id, this);
            if(player) {
                if((id == 'player1' && !this.data.part1Hide) || (id == 'player2' && !this.data.player2.isPlaying)) {return;};
                if(res.intersectionRatio == 0) {
                    // res.intersectionRatio == 0 表示不相交
                    player.pause();
                } else {
                    setTimeout(() => {player.play();}, 150);
                };
            };
        });
    },
    gifLoad(e) {
        let id = 'anGif' + e.currentTarget.id.slice(-1);
        this.animation.opacity(1).step({duration: 100, timingFunction: 'step-start'});
        this.setData({[id]: this.animation.export()});
    },
    showToast() {
        this.setData({toastHide: false});
        getApp().mtj.trackEvent('modalclick');
    },
    closeToast() {
        this.setData({toastHide: true});
    },
    // 跳其他小程序
    goCadillac() {
        wx.navigateToMiniProgram({
            appId: 'wx62f5a9e7afb6e1f6',
            path: 'pages/car/leaveInfo/leaveInfo?type=32',
            complete(res) {
                getApp().mtj.trackEvent('gocadillac');
            }
        });
    },
    goHummer() {
        getApp().mtj.trackEvent('gohummer');
        wx.navigateTo({url: '../hummerqr/hummerqr'});
    },
    // track
    trackObserve(id) {
        this._observer = this.createIntersectionObserver({observeAll: true});
        this._observer.relativeToViewport({bottom: -300}).observe('#' + id, (res) => {
            let n = id.substr(id.length - 1);
            if (res.intersectionRatio > 0 && !this.data.track['section3part' + n]) {
                getApp().mtj.trackEvent('section3part' + n);
                this.setData({['track.section3part' + n]: true});
            };
        });
    },
    // share
    onShareAppMessage(res) {
        let path = 'pages/index/index';
        if(getApp().isset(getApp().globalData.param)) {
            path = path + '?type=' + getApp().globalData.param;
        };
        return {
            title: '让电动车 生来更强',
            path: path,
            imageUrl: baseUrl + '/share.jpg',
        }
    },
    onShareTimeline(res) {
        let query = '';
        if(getApp().isset(getApp().globalData.param)) {
            query = 'type=' + getApp().globalData.param;
        };
        return {
            title: '让电动车 生来更强',
            query: query,
            imageUrl: baseUrl + '/share.jpg',
        }
    }
});
