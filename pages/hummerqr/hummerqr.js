// hummerqr.js
const baseUrl = getApp().globalData.baseUrl; // domain

Page({
    data: {
        sources: {
            bg: baseUrl + '/hummer/bg.jpg',
            title: baseUrl + '/hummer/title.png',
            qrcode: baseUrl + '/hummer/qrcode.png',
            tip2: baseUrl + '/hummer/tip2.png'
        },
        anQr: {}, // 整体动画
    },
    onLoad() {
        let animation = wx.createAnimation();
        this.animation = animation;
        setTimeout(() => {this.anQrStart();}, 600);
    },
    onReady() {},
    onShow() {},
    onHide() {},
    // an all
    anQrStart() {
        this.animation.opacity(1).step({duration: 300, timingFunction: 'ease-out'});
        this.setData({anQr: this.animation.export()});
    },
    anQrEnd() {
        this.setData({anQr: null});
        this.animation = null;
    },
    // save
    saveQr(e) {
        const url = e.currentTarget.dataset.url;
        const { saveImage1, saveImage2, saveImage3 } = this;
        this.saveImage1(url).then(saveImage2).then(saveImage3);
    },
    saveImage1(url) {
        const p1 = new Promise((resolve, reject) => {
            wx.getSetting({
                success(res) {
                    if(res.authSetting['scope.writePhotosAlbum'] == undefined) {
                        wx.authorize({
                            scope: 'scope.writePhotosAlbum',
                            success(res) {
                                resolve(url);
                            }
                        });
                    } else {
                        if(res.authSetting['scope.writePhotosAlbum']) {
                            resolve(url);
                        } else {
                            wx.showModal({
                                title: '保存失败',
                                content: '请开启访问手机相册权限',
                                success(res) {
                                    if(res.confirm) {
                                        wx.openSetting();
                                    };
                                }
                            });
                        };
                    };
                }
            });
        });
        return p1;
    },
    // 授权
    saveImage2(url) {
        const p2 = new Promise((resolve, reject) => {
            wx.getImageInfo({
                src: url,
                success(res) {
                    resolve(res.path);
                },
                fail(res) {
                    wx.showToast({title: '图片下载失败', icon: 'error'});
                }
            });
        });
        return p2;
    },
    // 转换图片格式为本地路径
    saveImage3(path) {
        const p3 = new Promise((resolve, reject) => {
            wx.saveImageToPhotosAlbum({
                filePath: path,
                success(res) {
                    wx.showToast({title: '已保存到相册'});
                },
                fail(res) {
                    wx.showToast({title: '图片保存失败', icon: 'error'});
                }
            });
        });
        return p3;
    },
    // share
    onShareAppMessage(res) {
        let path = 'pages/index/index';
        if(getApp().isset(getApp().globalData.param)) {
            path = path + '?type=' + getApp().globalData.param;
        };
        return {
            title: '让电动车 生来更强',
            path: path,
            imageUrl: baseUrl + '/share.jpg',
        }
    },
    onShareTimeline(res) {
        let query = '';
        if(getApp().isset(getApp().globalData.param)) {
            query = 'type=' + getApp().globalData.param;
        };
        return {
            title: '让电动车 生来更强',
            query: query,
            imageUrl: baseUrl + '/share.jpg',
        }
    }
})
