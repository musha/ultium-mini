// hummer.js
const baseUrl = getApp().globalData.baseUrl; // domain

Page({
    data: {
        sources: {
            bg: baseUrl + '/hummer/bg.jpg',
            title: baseUrl + '/hummer/title.png?_v=0fg7w7st',
            tip: baseUrl + '/hummer/tip.png'
        }
    },
    onLoad() {},
    onReady() {},
    onShow() {},
    onHide() {},
    // share
    onShareAppMessage(res) {
        let path = 'pages/index/index';
        if(getApp().isset(getApp().globalData.param)) {
            path = path + '?type=' + getApp().globalData.param;
        };
        return {
            title: '让电动车 生来更强',
            path: path,
            imageUrl: baseUrl + '/share.jpg',
        }
    },
    onShareTimeline(res) {
        let query = '';
        if(getApp().isset(getApp().globalData.param)) {
            query = 'type=' + getApp().globalData.param;
        };
        return {
            title: '让电动车 生来更强',
            query: query,
            imageUrl: baseUrl + '/share.jpg',
        }
    }
})
