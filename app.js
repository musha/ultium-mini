// app.js
const mtjwxsdk = require('./utils/mtj-wx-sdk.js'); // baidu统计

App({
    globalData: {
        baseUrl: 'https://uitium.uglobal.com.cn',
        param: null
    },
    onLaunch(options) {
        // check param
        if(this.isset(options.query.type)) {
            this.checkParam(options.query.type);
        };
        // check update
        this.checkUpdate();
        // share
        wx.showShareMenu({
            withShareTicket: true,
            menus: ['shareAppMessage', 'shareTimeline']
        });
    },
    onShow() {},
    onHide() {},
    // method
    isset(param) {
        if(param != '' && param != null && param != undefined) {
            return true;
        } else {
            return false;
        };
    },
    checkParam(param) {
        if(!this.isset(wx.getStorageSync(param))) {
            this.mtj.trackEvent(param);
            wx.setStorageSync(param, 1);
        };
        this.globalData.param = param;
    },
    checkUpdate() {
        const updateManager = wx.getUpdateManager();
        if(updateManager) {
            updateManager.onCheckForUpdate(res => {
                // 请求完新版本信息的回调
                // console.log(res.hasUpdate);
            });
            updateManager.onUpdateReady(() => {
                wx.showModal({
                    title: '更新提示',
                    content: '新版本已经准备好，是否重启应用？',
                    success: res => {
                        if(res.confirm) {
                            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                            updateManager.applyUpdate();
                        }
                    }
                });
            });
            updateManager.onUpdateFailed(() => {
                // 新版本下载失败
                console.log('新版本下载失败');
            });
        };
    }
});
